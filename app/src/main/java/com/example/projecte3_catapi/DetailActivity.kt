package com.example.projecte3_catapi

import android.os.Bundle
import android.util.Log
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.Text
import androidx.compose.material.lightColors
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.graphics.Color
import androidx.lifecycle.viewmodel.compose.viewModel
import com.example.projecte3_catapi.ui.screens.DetailViewModel
import com.example.projecte3_catapi.ui.theme.Projecte3_CatAPITheme
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.modifier.modifierLocalConsumer
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import coil.compose.rememberAsyncImagePainter

class DetailActivity : ComponentActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        val idCat= intent.getStringExtra("id")
        setContent {


               idCat?.let {  DetailApp(idCat =it)}

        }
    }



@Composable
fun DetailApp(idCat: String){
    Projecte3_CatAPITheme() {
        DetailCat(idCat)
    }
}


@Composable
fun DetailCat(idCat: String, viewModel: DetailViewModel = viewModel()) {

    val uiState by viewModel.uiState.collectAsState()
   if(uiState == null)
       viewModel.getDetail(idCat)

    uiState?.let { uiState->
        Column(
            horizontalAlignment = Alignment.CenterHorizontally,
            modifier = Modifier
                .background(Color.Yellow)
                .verticalScroll(rememberScrollState())
        ) {

            Text(
                text = uiState.name,
                fontSize = 18.sp,
                fontWeight = FontWeight.SemiBold,
                modifier = Modifier.padding(
                    bottom = 18.dp,
                    start = 18.dp,
                    top = 18.dp,
                    end = 18.dp
                )
            )

            Image(
                painter = rememberAsyncImagePainter(uiState.img_url),
                contentDescription = "Error, not image",
                modifier = Modifier
                    .clip(RoundedCornerShape(25))
                    .size(250.dp)

            )

            Text(
                text = "TEMPERAMENT" +uiState.temperament,
                fontSize = 13.sp,
                modifier = Modifier.padding(
                    bottom = 18.dp,
                    start = 18.dp,
                    top = 18.dp,
                    end = 18.dp
                )
            )

            Text(
                text ="COUNTRY CODE" + uiState.countryCode,
                fontSize = 13.sp,
                modifier = Modifier.padding(
                    bottom = 18.dp,
                    start = 18.dp,
                    top = 18.dp,
                    end = 18.dp
                )

                )

            Text(
                text = "DESCRIPTION" + uiState.description,
                fontSize = 13.sp,
                modifier = Modifier.padding(
                    bottom = 18.dp,
                    start = 18.dp,
                    top = 18.dp,
                    end = 18.dp
                )

                )

            Text(
                text = "WIKIPEDIA URL" + uiState.wikipediaUrl,
                fontSize = 13.sp,
                modifier = Modifier.padding(
                    bottom = 18.dp,
                    start = 18.dp,
                    top = 18.dp,
                    end = 18.dp
                )


                )

        }

    }


}


@Preview
@Composable
fun viewThis(){
    Image(
        painter = rememberAsyncImagePainter("https://cdn2.thecatapi.com/images/gCEFbK7in.jpg"),
        contentDescription = "Error, not image",
        modifier = Modifier
            .clip(RoundedCornerShape(25))
    )
}
}
