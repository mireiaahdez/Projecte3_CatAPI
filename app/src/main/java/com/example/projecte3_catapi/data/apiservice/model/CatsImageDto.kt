package com.example.projecte3_catapi.data.apiservice.model

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class CatsImageDto(
    @SerialName("url") val imageUrl: String ,
    @SerialName("id") val id : String

)
