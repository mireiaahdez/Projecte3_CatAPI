package com.example.projecte3_catapi.data.apiservice

import com.example.projecte3_catapi.data.apiservice.model.CatsImageDto
import com.example.projecte3_catapi.data.apiservice.model.BreedDto
import com.google.gson.Gson
import kotlinx.serialization.json.Json
import com.jakewharton.retrofit2.converter.kotlinx.serialization.asConverterFactory
import kotlinx.serialization.ExperimentalSerializationApi

import okhttp3.MediaType
import okhttp3.MediaType.Companion.toMediaType
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query


private const val BASE_URL = "https://api.thecatapi.com"
@OptIn(ExperimentalSerializationApi::class)
private val retrofit = Retrofit.Builder()
    .addConverterFactory(Json{this.ignoreUnknownKeys = true}.asConverterFactory(("application/json").toMediaType()))
    .baseUrl(BASE_URL)
    .build()

object CatsApi{
    val retrofitService : CatsApiService by lazy {
        retrofit.create(CatsApiService::class.java)
    }
}


interface CatsApiService {

    //funciones que llaman a la API

    @GET("/v1/breeds")
    suspend fun getBreeds(): List<BreedDto>

    @GET("/v1/images/search")
    suspend fun getCatsImage(@Query("breed_ids") id: String): List<CatsImageDto>

    @GET("/v1/breeds/{id}")
    suspend fun getBreed(@Path("id") id: String): BreedDto
}