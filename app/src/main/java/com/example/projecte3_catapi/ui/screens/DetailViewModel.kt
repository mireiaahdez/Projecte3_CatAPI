package com.example.projecte3_catapi.ui.screens

import android.util.Log
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.projecte3_catapi.data.apiservice.CatsApi
import com.example.projecte3_catapi.ui.model.CatsUIModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch

class DetailViewModel : ViewModel(){
    private val _uiState = MutableStateFlow<CatsUIModel?>(null)
    val uiState : StateFlow<CatsUIModel?> = _uiState.asStateFlow()

    fun getDetail(id:String) = viewModelScope.launch {
        val cat = CatsApi.retrofitService.getBreed(id)
        val image = CatsApi.retrofitService.getCatsImage(id)

        _uiState.value = CatsUIModel(
            id = cat.id,
            img_url = image.first().imageUrl,
            wikipediaUrl = cat.wikipediaUrl,
            name = cat.name,
            description = cat.description,
            countryCode = cat.countryCode,
            temperament = cat.temperament
        )
    }


}