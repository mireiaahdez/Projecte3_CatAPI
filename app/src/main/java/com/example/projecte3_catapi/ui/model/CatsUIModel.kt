package com.example.projecte3_catapi.ui.model

data class CatsUIModel(
    val id : String,
    val img_url : String,
    val name : String,
    val temperament :String,
    val countryCode : String,
    val description : String,
    val wikipediaUrl : String
)