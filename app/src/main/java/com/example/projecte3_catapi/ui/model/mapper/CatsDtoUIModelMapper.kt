package com.example.projecte3_catapi.ui.model.mapper

import androidx.compose.ui.res.booleanResource
import com.example.projecte3_catapi.data.apiservice.model.BreedDto
import com.example.projecte3_catapi.data.apiservice.model.CatsImageDto
import com.example.projecte3_catapi.ui.model.CatsUIModel

class CatsDtoUIModelMapper {

fun map (breeds: List<BreedDto>, images : List<CatsImageDto>): List<CatsUIModel> {
        return images.mapIndexed { index, images ->
            CatsUIModel(
                id= breeds[index].id,
                img_url= images.imageUrl,
                name = breeds[index].name,
                temperament =  breeds[index].temperament,
                countryCode = breeds[index].countryCode,
                description = breeds[index].description,
                wikipediaUrl = breeds[index].wikipediaUrl
            )
        }

}
}
//breeds.zip(images).flatMap{ (it.first,it.second)}+breeds.drop(images.size)
