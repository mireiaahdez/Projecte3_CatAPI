package com.example.projecte3_catapi.ui.screens

import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.projecte3_catapi.data.apiservice.CatsApi
import com.example.projecte3_catapi.ui.model.CatsUIModel
import com.example.projecte3_catapi.ui.model.mapper.CatsDtoUIModelMapper
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch

class CatsViewModel : ViewModel(){

    private val _uiState = MutableStateFlow<List<CatsUIModel>>(emptyList())
    val uiState : StateFlow<List<CatsUIModel>> = _uiState.asStateFlow()

    var mapper = CatsDtoUIModelMapper()

    init{
        viewModelScope.launch {
            val breeds = CatsApi.retrofitService.getBreeds()
            val images = breeds.flatMap { breed ->
                CatsApi.retrofitService.getCatsImage(breed.id)
            }
            _uiState.value = mapper.map(breeds, images)












































































































































































        }
    }



}