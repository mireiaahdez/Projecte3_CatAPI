package com.example.projecte3_catapi

import android.content.Intent
import android.os.Bundle
import android.widget.Space
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.animation.animateContentSize
import androidx.compose.animation.core.Spring
import androidx.compose.animation.core.spring
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.*
import androidx.compose.material.MaterialTheme.colors
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ExpandLess
import androidx.compose.material.icons.filled.ExpandMore
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.lifecycle.viewmodel.compose.viewModel
import coil.compose.rememberAsyncImagePainter
import com.example.projecte3_catapi.ui.model.CatsUIModel
import com.example.projecte3_catapi.ui.screens.CatsViewModel
import com.example.projecte3_catapi.ui.theme.Projecte3_CatAPITheme

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            Projecte3_CatAPITheme {
                // A surface container using the 'background' color from the theme
              CatsApp()
            }
        }
    }

   
}

private const val TAG = "MainActivity"

@Composable
fun CatsApp (catsViewModel: CatsViewModel = viewModel()){
    val uiState by catsViewModel.uiState.collectAsState()
    CatList(catList = uiState)
}

@Composable
fun CatList(catList: List<CatsUIModel>, modifier: Modifier = Modifier) {
    LazyColumn(
        modifier = Modifier
            .fillMaxSize(),
            contentPadding = PaddingValues(14.dp)
    ){
        items(catList){ element ->
            CatItemCard(catsUIModel =  element)
            Spacer(modifier = Modifier.size(13.dp))
        }
    }
}


@Composable
fun CatItemCard(catsUIModel: CatsUIModel, modifier: Modifier = Modifier) {
    var expanded by remember{
        mutableStateOf(false)
    }

    Card(
        backgroundColor = Color(0xFFBB86FC),
        modifier = Modifier.fillMaxWidth(),
        shape = RoundedCornerShape(14.dp)
    ){
        Column (
            modifier = Modifier.animateContentSize (
                animationSpec = spring(
                    dampingRatio = Spring.DampingRatioMediumBouncy,
                    stiffness = Spring.StiffnessLow
                )
            )
                )
        {
            Row (
                verticalAlignment = Alignment.CenterVertically
            ){
                Image(
                    painter = rememberAsyncImagePainter(catsUIModel.img_url),
                    contentDescription =  null,
                    modifier = Modifier
                        .size(100.dp)
                        .clip(RoundedCornerShape(100))
                        .padding(horizontal = 10.dp)
                )

                Text(
                    text = catsUIModel.name,
                    modifier = Modifier.weight(1.5f)
                )
                ButtonCard(expanded = expanded, onClick = { expanded= !expanded })

            }
            if (expanded){
                CatDescription(catsUIModel.description, catsUIModel.id)
            }

        }
    }
}


@Composable
fun CatDescription(description : String, catId: String ) {
    Column(
        modifier = Modifier.padding(
            start = 18.dp,
            top = 9.dp,
            end = 18.dp,
            bottom = 18.dp
        )
    ) {
        Text(
            text = description,
            overflow = TextOverflow.Ellipsis,
            maxLines = 2
        )

    }
    Spacer(modifier = Modifier)
    val context = LocalContext.current

    Button(
        onClick = {
            val intent = (Intent(context, DetailActivity::class.java))
            intent.putExtra("id",catId)
            context.startActivity(intent)
        },
        modifier = Modifier
            .padding(16.dp)

    ) {
        Text(
            text = "See More",
            fontSize = 12.sp,
            fontWeight = FontWeight.Bold
        )
    }

}

@Composable
fun ButtonCard(expanded: Boolean, onClick: () -> Unit, modifier: Modifier = Modifier) {

    IconButton(onClick = onClick){

        Icon(
            imageVector = if (expanded) Icons.Filled.ExpandLess
                          else Icons.Filled.ExpandMore,
            contentDescription = null,
            tint = MaterialTheme.colors.secondary

        )

    }
}



